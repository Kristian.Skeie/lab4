package cellular;

import datastructure.IGrid;

import java.util.Random;

import datastructure.CellGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns){
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

	@Override
	public CellState getCellState(int row, int col) {
		// TODO Auto-generated method stub
		return currentGeneration.get(row, col);
	}

	@Override
	public void initializeCells() {
        // TODO 
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for(int row = 0; row < currentGeneration.numRows(); row ++){
			for (int col = 0; col < currentGeneration.numColumns(); col++){
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO
		if(this.getCellState(row, col).equals(CellState.ALIVE)){
            return CellState.DYING;
        }
        if(this.getCellState(row, col).equals(CellState.DYING)){
            return CellState.DEAD;
        }
        if(this.countNeighbors(row, col, CellState.ALIVE) == 2){
            return CellState.ALIVE;
        }
        return getCellState(row, col);
	}

	@Override
	public int numberOfRows() {
		// TODO Auto-generated method stub
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO Auto-generated method stub
		return currentGeneration.numColumns();
	}

	@Override
	public IGrid getGrid() {
		// TODO Auto-generated method stub
		return currentGeneration;
	}

    private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int count = 0;
		for(int ckRow = row-1; ckRow <= row+1; ckRow++){
			if(ckRow>=0 && ckRow<=currentGeneration.numRows()){
				for(int ckCol = col-1; ckCol <= col+1; ckCol++){
					if(ckCol>=0 && ckCol<=currentGeneration.numColumns()){
						if(getCellState(ckRow, ckCol).equals(CellState.ALIVE)){
							count++;
						}
					}
					
				}
			}
			
		}
		if(getCellState(row, col).equals(CellState.ALIVE)){
			count--;
		}
		return count;
		
	}
    
}

