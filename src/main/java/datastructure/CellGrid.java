package datastructure;

import java.util.ArrayList;

import javax.swing.event.CellEditorListener;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int columns;
    private int rows;
    private CellState initialState;
    //initialize a two-dimensional space-holder
    private CellState[][] cellStates; 

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.columns = columns;
        this.rows = rows;
        this.initialState = initialState;
        cellStates = new CellState[rows][columns]; 
        for(int row = 0; row < rows; row++){
            for(int col = 0; col < columns; col++){
                cellStates[row][col] = initialState;
            }
        }
        
	}   

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return cellStates.length;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cellStates[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if(row <0 || row >= rows){
            throw new IndexOutOfBoundsException("WRONG ROW INDEX-VALUE1");
        }
        if(column < 0 || column >= columns){
            throw new IndexOutOfBoundsException("Wrong column index1");
        }
        cellStates[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if(row < 0 || row >= rows){
            throw new IndexOutOfBoundsException("Wrong row index2");
        }
        if(column < 0 || column >= columns){
            throw new IndexOutOfBoundsException("Wrong column index2");
        }
        return cellStates[row][column];
        
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid gridCopy = new CellGrid(numRows(), numColumns(), initialState);
        for(int row = 0; row < rows; row++){
            for(int col = 0; col < columns; col++){
                CellState state = get(row, col);
                gridCopy.set(row, col, state);
            }
        }
        return gridCopy;
    }
    
}
